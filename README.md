## CSMC207

# Basic Functions
* Controllers
* Template Parser
* Model
* Database Handler

# Installation Notes

Run `composer install`

# Requirements
* PHP 7.x
* MySQL 5.7.x
